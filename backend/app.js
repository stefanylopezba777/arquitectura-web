const express = require('express')
const cors = require('cors')
const app = express()

//end-point de la aplicacion
const routerAuth = require("./routes/auth");
const routerUser = require("./routes/user");
const routerNote = require("./routes/note");

const conectarDB = require('./database/conectDB');
app.use(cors())

app.use(express.json())
app.use('/api/auth',routerAuth)
app.use('/api/user',routerUser)
app.use('/api/note',routerNote)

app.listen(4000,()=>{
  console.log('Servidor corriendo en el puerto ',4000);
})
conectarDB()
