const User = require("../models/User")

const login = async (req, res) => {
	if (!req.body) {
		return res.status(400).json({ msg: "User y pass obligatorios" })
	}
	const { email, password } = req.body

	if (email === null || password === null) {
		return res.json({
			msg: "Todos los campos son obligatorios",
		})
	}
	const usuario = await User.findOne({email})

	if(!usuario){
		return res.status(402).json({
			msg: "Usuario no existe",
		})
	}

	if ( usuario.password !== password) {
		return res.status(400).json({
			msg: "Datos incorrectos",
		})
	}
  let token= Math.random(5).toString(36).substring(0,10);
	return res.status(200).json({
		usuario,
		msg: "Usuario logueado correctamente!",
    token
	})
}
module.exports = login
