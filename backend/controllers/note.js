const Note = require("../models/Note")

const createNote = async (req, res) => {
	const note = new Note(req.body)

	try {
		const noteAdded = await note.save()
		res.status(200).json({
			msg: "Nota creado correctamente",
			noteAdded,
		})
	} catch (error) {
		console.error(error.message)
		res.status(500).json({
			msg: error.message,
		})
	}
}
const obtenerNotes = async (req, res) => {
	try {
		const notes = await Note.find()
		res.status(200).json({
			notes,
		})
	} catch (error) {
		console.log(error)
		res.status(500).json({
			msg: error.message,
		})
	}
}
const obtenerNotesById = async (req, res) => {
	const { id } = req.params
	try {
		const notes = await Note.find( {user: id} )

		res.status(200).json({
			notes,
		})
	} catch (error) {
		console.log(error)
		res.status(500).json({
			msg: "Ocurrio un error inesperado",
		})
	}
}
const eliminarNote = async (req,res)=>{
	const {id} = req.params
	try {
		const note = await Note.findByIdAndRemove(id)
		res.status(200).json({
			msg:'Nota eliminada correctamente'
		})
	} catch (error) {
		return res.status(500).json({
			msg:error.message
		})
	}

}
const actualizarNote = async (req,res)=>{
	const {id} = req.params
	try {
		const note =  await Note.findByIdAndUpdate(id,req.body)
		res.status(200).json({
			msg:'Nota actualizada correctamente!'
		})
	} catch (error) {
		res.status(500).json({
			msg:error.message
		})
	}
}
module.exports = {
	createNote,
	obtenerNotes,
	obtenerNotesById,
	eliminarNote,
	actualizarNote
}
