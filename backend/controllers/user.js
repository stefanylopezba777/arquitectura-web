const User = require("../models/User")

const createUser = async (req,res)=>{
  
  const usuario = new User(req.body)
  try {
    const usuarioGuardado = await usuario.save()
    res.status(200).json({
      usuarioGuardado,
      msg:'Usuario guardado correctamente'
    })
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg:'Ocurrio un error inesperado'
    })
    
  }

}

module.exports ={
  createUser
}