const { default: mongoose } = require("mongoose")

const MONGO_URI = 'mongodb+srv://lizzbareiro:lrp67kohdX3xkCi5@cluster0.z3ifq.mongodb.net/'
const conectarDB = async () => {

	try {
		const db = await mongoose.connect(MONGO_URI, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
		})
		const url = `${db.connection.host}:${db.connection.port}`
		console.log(`Base datos conectado ${url}`)
	} catch (error) {
		console.log(error)
		process.exit(1)
	}
}
module.exports =  conectarDB