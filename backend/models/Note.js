const mongoose = require('mongoose')

const NoteSchema = mongoose.Schema({
  title:{
    type:String,
    required: [true,'La nota debe tener un titulo']
  },
  task:{
    type:String,
    required:[true,'La nota debe tener una tarea']
  },
  fecha:{
    type:Date
  },
  type:{
    type:String,
    emun:['recordatorio','actividades','pendientes']

  },
  user:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'user'
  }


})
const Note = mongoose.model('note',NoteSchema)
module.exports = Note