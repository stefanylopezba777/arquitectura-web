const mongoose = require('mongoose')

const UserSchema = mongoose.Schema({
  name:{
    type:String,
    required: [true,'El nombre de usuario es obligatorio']
  },
  email:{
    type:String,
    required:[true,'El correo es obligatorio']
  },
  password:{
    type:String,
    required:[true,'La contrasena   es obligatorio']

  }


})
const User = mongoose.model('user',UserSchema)
module.exports = User