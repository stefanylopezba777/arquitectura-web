const express = require('express')
const login = require('../controllers/auth')
const { createNote, obtenerNotes, obtenerNotesById, eliminarNote, actualizarNote } = require('../controllers/note')
const router = express.Router()

router.post('/',createNote)
router.delete('/:id',eliminarNote)
router.put('/:id',actualizarNote)
router.get('/:id',obtenerNotesById)





module.exports =  router