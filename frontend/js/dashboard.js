//elementos del html
const contenedorNotas = document.getElementById("listado-notes")
const formulario = document.getElementById("formulario")
const title = document.getElementById("form-titulo")
const task = document.getElementById("text-area")
const fecha = document.getElementById("fecha")
const btnAgregar = document.getElementById("btn-agregar")
let modoEdicion = false;
let idNote;
//endpoint
const titulo = document.getElementById("titulo")
const API_NOTE = "http://localhost:4000/api/note/"
let API_NOTE_Editar = `http://localhost:4000/api/note/`

let notes = []
let user = {}


//eventos
document.addEventListener("DOMContentLoaded", async () => {
	user = JSON.parse(localStorage.getItem("user"))
	titulo.textContent = `Bienvenido/a ${user.name} !`
	await cargarTarea()
	
	
})

formulario.addEventListener("submit", async (e) => {
	e.preventDefault()
	
	if(title.value === "" || task.value === "" || fecha.value === ""){
		return swal({
			title: "Atencion!",
			text: 'Todos los campos son obligatorios!',
			icon: "warning",
		})
	}

	const formRes = {
		title: title.value,
		task: task.value,
		fecha:fecha.value,
		type: "actividades",
		user: user._id,
	}
	
	console.log(formRes)
	if(modoEdicion === false){
		try {
			const req = await fetch(API_NOTE, {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
					},
					body: JSON.stringify(formRes),
				}),
				res = await req.json()
			console.log(res)
	
			if (!req.ok) {
				throw new Error(res.msg)
			}
			swal({
				title: "Nota almacenada",
				text: res.msg,
				icon: "success",
			})
			formulario.reset()
			//await cargarTarea()
			setTimeout(()=>{
				window.location.reload()
		},1000)
			return
		} catch (error) {
			console.log(error)
			swal({
				title: "Ocurrio un error",
				text: error.message,
				icon: "warning",
			})
		}
	}
	try {
		console.log('Haciendo peticiona ',API_NOTE_Editar+idNote);
		const req = await fetch(API_NOTE_Editar+idNote, {
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify(formRes),
			}),
			res = await req.json()
		

		if (!req.ok) {
			throw new Error(res.msg)
		}
		swal({
			title: "Nota almacenada",
			text: res.msg,
			icon: "success",
		})
		formulario.reset()
		//await cargarTarea()
		setTimeout(()=>{
			window.location.reload()
	},1000)
		return
	} catch (error) {
		console.log(error)
		swal({
			title: "Ocurrio un error",
			text: error.message,
			icon: "warning",
		})
	}
	
})

//verifica si el usuario tiene tareas
//en caso de que tenga obtiene todas las tareas
async function cargarTarea() {
	if (!user) {
		return
	}
	try {
		const req = await fetch(`${API_NOTE}${user._id}`),
			res = await req.json()
		notes =  res.notes
		if(!req.ok){

		}
		mostrarNotes()
	} catch (error) {
		console.log(error)
		return
	}
}

function mostrarNotes() {
	if (notes.length <= 0) {
		let contenedorRepsuesta = document.createElement("div")
			let tituloNotes = document.createElement("h6")

			tituloNotes.classList.add('text-title','uppercase')
			tituloNotes.innerHTML= `Sin notas asignadas`
			contenedorRepsuesta.appendChild(tituloNotes)
			contenedorNotas.appendChild(contenedorRepsuesta)
	} else {
		notes.forEach((tarea) => {
			console.log(tarea);
			const { title, task } = tarea
			//let newFecha = fecha.slice(0,10)
			let contenedorRepsuesta = document.createElement("div")
			let btnEditar = document.createElement('button')
			let btnEliminar = document.createElement('button')
			let parrafoNotes = document.createElement("p")
			let tituloNotes = document.createElement("h6")

			tituloNotes.classList.add('text-title','text-uppercase')
			tituloNotes.innerHTML= `<b>${title}</b>`
			parrafoNotes.innerHTML = `<b>Nota:</b>${task} <br><b>Fecha: </b>`
			btnEditar.classList.add('btn','btn-dark','btn-sm','mx-2')
			btnEditar.textContent = 'Editar'
			btnEditar.onclick = ()=>editarNote(tarea)
			btnEliminar.classList.add('btn','btn-danger','btn-sm')
			btnEliminar.textContent = 'Eliminar'
			btnEliminar.onclick = ()=>eliminarNote(tarea._id)
			contenedorRepsuesta.appendChild(tituloNotes)
			contenedorRepsuesta.appendChild(parrafoNotes)
			contenedorRepsuesta.appendChild(btnEditar)
			contenedorRepsuesta.appendChild(btnEliminar)
			contenedorRepsuesta.classList.add('shadow','p-3','rounded','mt-2')
			contenedorNotas.appendChild(contenedorRepsuesta)
		})
	}
}

async function eliminarNote(id) {
	
	try {
		const req = await fetch(`${API_NOTE}/${id}`, {
			method: "DELETE",
			headers: {
				"Content-Type": "application/json",
			},
			
		}),
		res = await req.json()
		
	
		if (!req.ok) {
			throw new Error(res.msg)
		}
		swal({
			title: "Nota eliminada correctamente",
			text: res.msg,
			icon: "success",
		})

		setTimeout(()=>{
				window.location.reload()
		},1000)
	} catch (error) {
		return alert(error)		
	}
	

	
}

function editarNote(note){
	modoEdicion = true
	btnAgregar.textContent = 'Guardar cambios'
	
	idNote = note._id
	fecha.value = note.fecha
	title.value = note.title
	task.value = note.task

}