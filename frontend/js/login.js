//Obtenemos datos del formulario 
const formulario = document.getElementById("formulario")
const email = document.getElementById("correo")
const password = document.getElementById("password")

//endpoint
const API_AUTH = 'http://localhost:4000/api/auth'

formulario.addEventListener("submit", auth)


async function auth(e) {
	e.preventDefault()
  const formularioValidado = validarFormulario()
  if(formularioValidado.error){
    return swal({
      title: "Ocurrio un error!",
      text: formularioValidado.mensaje,
      icon: formularioValidado.tipo,
    })
  }

  const credenciales = {email:email.value,password:password.value}
  
  //llamamos al EndPoint
  const req = await fetch(API_AUTH,{
    method:"POST",
    headers:{
      "content-type":"application/json"
    },
    body:JSON.stringify(credenciales)
  }),
  res = await req.json()
  
  //verificamos si hubo error 
  if(!req.ok){
    console.log(res);
    return swal({
      title:'Ocurrio un error',
      text:res.msg,
      icon:'warning'
    })
  }
  
  //Si las credenciales coinciden 
  swal({
    title:'Credenciales correctas',
    text:res.msg,
    icon:'success'
  })

  localStorage.setItem('user',JSON.stringify(res.usuario) )
  localStorage.setItem('token',JSON.stringify(res.token) )
  //redireccionamos al Dashboard 
  setTimeout(()=>{

    window.location.href = '/frontend/Dashboard.html' 
  },1000)

}

function validarFormulario() {
  const formularioVerificado ={tipo : '',mensaje: '',error:false}
	if (email.value === "" || password.value === "") {
		formularioVerificado.tipo = 'warning'
		formularioVerificado.error = true
		formularioVerificado.mensaje = 'Todos los campos son obligatorios'
    return formularioVerificado
  }else{
    formularioVerificado.tipo = 'success'
		formularioVerificado.error = false
		formularioVerificado.mensaje = 'formulario validado'
    return formularioVerificado

  }
    




}


